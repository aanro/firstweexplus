// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 49);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var host = 'http://59.110.169.246/movie/';
// var host='http://192.168.1.101:8080/'


exports.default = {

    postShort: function postShort(weg, param, header, start, success, compelete) {
        var modal = weex.requireModule("modal");
        this.postFull(weg, param, header, start, success, function (res) {
            //fail
            modal.toast({ message: res.msg });
        }, function () {
            //exception
            modal.toast({ message: '网络异常！' });
        }, function () {
            //compelete

            compelete();
        });
    },

    postFull: function postFull(weg, param, header, start, success, fail, exception, compelete) {
        var net = weex.requireModule("net");
        var modal = weex.requireModule("modal");
        var self = this;
        var url = host + weg;
        var st = weex.requireModule('static');
        var token = st.getString('token');
        if (token != undefined && token != '') {
            header.token = token;
        }
        // param.token='95d594d7b18fd1c7db37e81dd5bae9c9'
        net.post(url, param, header, function () {
            //start
            start();
        }, function (e) {
            //success
            // modal.toast({message:e.res.err})
            if (e.res.err == 0) {

                success(e.res);
            } else {
                // modal.toast({message:e.res.msg})
                if (token != undefined && token != '') {
                    st.remove('token');
                    return;
                }
                if (e.res.err == 1000) {
                    // var nav=weex.requireModule("navigator")
                    // nav.presentFull('root:busi/account/login.js',{},'transparent',true,function(){
                    //     self.postFull(weg,param,header,start,success,fail,exception,compelete);

                    // },true);
                } else fail(e.res);
            }
        }, function (e) {
            //compelete


            compelete();
        }, function (e) {
            // exception
            exception();
        });
    },

    post: function post(weg, param, success) {
        var progress = weex.requireModule("progress");
        this.postShort(weg, param, {}, function () {
            progress.show();
        }, success, function () {
            progress.dismiss();
        });
    },

    postSilent: function postSilent(weg, param, success) {

        this.postFull(weg, param, {}, function () {}, success, function (res) {
            //fail

        }, function () {
            //exception

        }, function () {
            //compelete


        });
    }

};

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = {
  "bg": {
    "backgroundColor": "#f5f5f5"
  },
  "cell": {
    "height": 100,
    "backgroundColor": "#ffffff",
    "flexDirection": "row",
    "alignItems": "center",
    "borderRadius": 5
  },
  "arrow": {
    "width": 16,
    "height": 26
  },
  "font_normal": {
    "fontSize": 30
  },
  "theme_color": {
    "color": "#1296db"
  },
  "theme_bg": {
    "color": "#1296db"
  },
  "mask": {
    "backgroundColor": "#000000",
    "opacity": 0.6,
    "position": "absolute",
    "left": 0,
    "top": 0,
    "bottom": 0,
    "right": 0
  }
}

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = {
  "layout": {
    "backgroundColor": "#333333",
    "height": 128,
    "width": 750,
    "flexDirection": "row",
    "alignItems": "center",
    "justifyContent": "center"
  }
}

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


exports.default = {
    props: {
        title: {
            default: ''

        },
        back: {
            default: true
        },
        bgcolor: {
            default: '#222222'

        },
        isloading: {
            default: false
        },
        disabled: {

            default: false
        },

        type: {
            type: String,
            default: 'text'
        },
        font_size: {
            default: 20
        },
        height: {
            default: 128
        },
        top: {
            default: 40
        },
        titletop: {
            default: 10
        }

    },
    data: function data() {
        return {};
    },

    methods: {
        titleClick: function titleClick() {
            this.$emit('titleClick');
        },
        rightclick: function rightclick() {
            this.$emit('rightClick');
        },
        backTo: function backTo() {
            if (!this.back) {
                this.$emit('leftClick');
                return;
            }

            var nav = weex.requireModule("navigator");
            nav.back();
        },
        onclick: function onclick() {
            if (!this.disabled) this.$emit('onclick');
        },
        adjust: function adjust() {
            if (weex.config.env.platform == 'android') {
                //                    if(weex.config.env.osVersion=)
                var p = weex.config.env.osVersion;
                p = p.replace(/\./g, '');
                if (p.length < 3) p = p + "0";
                if (p <= '440') {
                    this.height = 108;
                    this.top = 16;
                    this.titletop = 4;
                }
            }
        }
    },

    created: function created() {

        this.adjust();
    },
    ready: function ready() {}
    //        watch: {
    //
    //
    //            disabled:{
    //                immediate: true,
    //                handler (val) {
    //
    //                }
    //            }
    //        }
};

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["layout"],
    style: {
      'background-color': _vm.bgcolor,
      'height': _vm.height
    }
  }, [_c('div', {
    staticStyle: {
      flexDirection: "row"
    },
    style: {
      'top': _vm.titletop
    }
  }, [(_vm.isloading) ? _c('div', {
    staticStyle: {
      height: "40",
      width: "40",
      marginRight: "10"
    }
  }) : _vm._e(), _c('text', {
    staticStyle: {
      flex: "1",
      color: "#ffffff",
      textAlign: "center",
      fontSize: "38"
    },
    on: {
      "click": _vm.titleClick
    }
  }, [_vm._v(_vm._s(_vm.title))]), (_vm.isloading) ? _c('floading', {
    staticStyle: {
      height: "40",
      width: "40",
      marginLeft: "10",
      marginTop: "5"
    },
    attrs: {
      "color": "#ffffff",
      "loadingStyle": "small"
    }
  }) : _vm._e()], 1), _c('div', {
    staticStyle: {
      width: "200",
      top: "40",
      position: "absolute",
      left: "0"
    },
    style: {
      'height': _vm.height,
      'top': _vm.top
    },
    on: {
      "click": _vm.backTo
    }
  }, [(_vm.back) ? _c('image', {
    staticStyle: {
      width: "80",
      height: "80"
    },
    attrs: {
      "src": "root:img/back.png"
    }
  }) : _vm._e(), _vm._t("left")], 2), _c('div', {
    staticStyle: {
      width: "200",
      position: "absolute",
      right: "0",
      top: "0",
      alignItems: "center",
      justifyContent: "center"
    },
    style: {
      'height': _vm.height
    },
    on: {
      "click": _vm.rightclick
    }
  }, [_vm._t("right")], 2), _c('div', {
    staticStyle: {
      height: "1",
      backgroundColor: "#111111",
      position: "absolute",
      bottom: "0",
      left: "0",
      right: "0"
    }
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(1)
)
__vue_styles__.push(__webpack_require__(2)
)

/* script */
__vue_exports__ = __webpack_require__(3)

/* template */
var __vue_template__ = __webpack_require__(4)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/component/header.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-c25810c0"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {

    login: function login(user, pass, comp) {

        var net = __webpack_require__(0);
        net.post('login.do', { name: user, pass: pass }, function (e) {

            var st = weex.requireModule('static');

            st.set('user', e.user);

            st.setString('token', e.user.token);
            comp(e);
        });
    },
    checkDo: function checkDo(success) {
        var navigator = weex.requireModule('navigator');
        navigator.present('root:busi/account/login.js', {}, 'transparent', true, function () {
            success();
        }, true);
    },
    isLogin: function isLogin() {
        var st = weex.requireModule('static');
        var usr = st.get('user');
        // var modal=weex.requireModule('modal');
        // modal.toast({message:usr})
        if (usr != undefined && usr != '') return true;
        return false;
    }

};

/***/ }),
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */
/***/ (function(module, exports) {

module.exports = {
  "bg": {
    "backgroundColor": "#f5f5f5"
  },
  "cell": {
    "height": 100,
    "backgroundColor": "#ffffff",
    "flexDirection": "row",
    "alignItems": "center",
    "borderRadius": 5
  },
  "arrow": {
    "width": 16,
    "height": 26
  },
  "font_normal": {
    "fontSize": 30
  },
  "theme_color": {
    "color": "#1296db"
  },
  "theme_bg": {
    "color": "#1296db"
  },
  "mask": {
    "backgroundColor": "#000000",
    "opacity": 0.6,
    "position": "absolute",
    "left": 0,
    "top": 0,
    "bottom": 0,
    "right": 0
  }
}

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = {
  "text": {
    "color": "#ffffff",
    "fontSize": 30
  },
  "text-disabled": {
    "color": "#b4b4b4",
    "fontSize": 30
  },
  "button": {
    "height": 100,
    "backgroundColor": "#1296db",
    "alignItems": "center",
    "justifyContent": "center",
    "color": "#ffffff",
    "borderRadius": 8,
    "backgroundColor:active": "#221edb"
  },
  "button-disabled": {
    "height": 100,
    "backgroundColor": "#eeeeee",
    "alignItems": "center",
    "justifyContent": "center",
    "color": "#ffffff",
    "borderRadius": 8,
    "backgroundColor:active": "#eeeeee"
  }
}

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//


exports.default = {
    props: {
        text: {
            type: String

        },
        color: {
            type: String

        },
        disabled: {

            default: false
        },

        type: {
            type: String,
            default: 'text'
        },
        font_size: {
            default: 20
        }

    },
    data: function data() {
        return {

            visiable: true

        };
    },

    methods: {
        oninput: function oninput(e) {

            //                this.$emit('oninput');
            this.$emit('oninput', e);
            this.visiable = e.value != '';
        },
        onclick: function onclick() {
            if (!this.disabled) this.$emit('onclick');
        },
        panstart: function panstart() {
            if (!this.disabled) this.bgcolor = '#1296db';
        },
        panend: function panend() {
            if (!this.disabled) this.bgcolor = '#1448db';
        },
        setenable: function setenable() {},
        onclose: function onclose() {
            this.value = '';
        }
    },

    created: function created() {

        this.visiable = !this.value == '';
    },
    ready: function ready() {}
    //        watch: {
    //
    //
    //            disabled:{
    //                immediate: true,
    //                handler (val) {
    //
    //                }
    //            }
    //        }
};

/***/ }),
/* 22 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    class: [_vm.disabled ? 'button-disabled' : 'button'],
    on: {
      "click": _vm.onclick
    }
  }, [_c('text', {
    class: [_vm.disabled ? 'text-disabled' : 'text']
  }, [_vm._v(_vm._s(_vm.text))])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(19)
)
__vue_styles__.push(__webpack_require__(20)
)

/* script */
__vue_exports__ = __webpack_require__(21)

/* template */
var __vue_template__ = __webpack_require__(22)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/component/button.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-3e5461c5"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var host = 'http://191.168.2.117:8080/';
function getScreenHeight() {

    return 750 / weex.config.env.deviceWidth * weex.config.env.deviceHeight;
}

/***/ }),
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(50)
)
__vue_styles__.push(__webpack_require__(51)
)

/* script */
__vue_exports__ = __webpack_require__(52)

/* template */
var __vue_template__ = __webpack_require__(53)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/account/login.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-7da79c16"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),
/* 50 */
/***/ (function(module, exports) {

module.exports = {
  "bg": {
    "backgroundColor": "#f5f5f5"
  },
  "cell": {
    "height": 100,
    "backgroundColor": "#ffffff",
    "flexDirection": "row",
    "alignItems": "center",
    "borderRadius": 5
  },
  "arrow": {
    "width": 16,
    "height": 26
  },
  "font_normal": {
    "fontSize": 30
  },
  "theme_color": {
    "color": "#1296db"
  },
  "theme_bg": {
    "color": "#1296db"
  },
  "mask": {
    "backgroundColor": "#000000",
    "opacity": 0.6,
    "position": "absolute",
    "left": 0,
    "top": 0,
    "bottom": 0,
    "right": 0
  }
}

/***/ }),
/* 51 */
/***/ (function(module, exports) {

module.exports = {
  "a": {
    "width": 200,
    "height": 200
  },
  "cell": {
    "height": 105,
    "alignItems": "center",
    "flexDirection": "row",
    "borderBottomWidth": 1.5,
    "borderBottomColor": "#eeeeee"
  },
  "text": {
    "color": "#929292",
    "fontSize": 30
  },
  "input": {
    "height": 105,
    "flex": 1,
    "paddingLeft": 30,
    "fontSize": 30
  },
  "title": {
    "fontSize": 33
  }
}

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var button = __webpack_require__(36);
var head = __webpack_require__(5);
var _login = __webpack_require__(14);
var util = __webpack_require__(37);

exports.default = {
    components: { button: button, head: head },
    data: {
        user: '',
        pass: ''
    },
    methods: {
        update: function update(e) {
            this.target = 'Weex';
            console.log('target:', this.target);
        },
        regist: function regist() {
            var nav = weex.requireModule("navigator");
            nav.push('regist.js');
        },
        leftClick: function leftClick() {
            var nav = weex.requireModule("navigator");
            nav.dismiss();
        },
        gotoAddress: function gotoAddress() {
            var navigator = weex.requireModule('navigator');
            navigator.push('../address/addAddress.js');
        },
        login: function login() {
            var modal = weex.requireModule('modal');
            var navigator = weex.requireModule('navigator');
            var modal = weex.requireModule("modal");

            var self = this;

            if (this.user == '') {
                modal.toast({ message: '请输入用户名!' });
                return;
            }
            if (this.pass == '') {
                modal.toast({ message: '请输入密码!' });
                return;
            }

            var net = weex.requireModule("net");
            var modal = weex.requireModule("modal");

            _login.login(this.user, this.pass, function (res) {

                //                modal.toast({message:'登录成功!'});
                var notify = weex.requireModule("notify");
                notify.send('login', res);
                navigator.dismissFull(res, true);
                var pref = weex.requireModule("pref");
                pref.set('user', self.user);
                pref.set('pass', self.pass);
                notify.send('tabselect', { index: 3 });
                //                navigator.invokeNativeCallBack(res);

                //                  navigator.push('../tab/mine.js')
            });
        }
    },

    created: function created() {

        var self = this;
        var globalEvent = weex.requireModule('globalEvent');
        var navigator = weex.requireModule('navigator');
        navigator.addBackGestureSelfControl();
        var pref = weex.requireModule("pref");
        this.user = pref.get('user');
        this.pass = pref.get('pass');
        globalEvent.addEventListener("onPageInit", function (e) {

            var nav = weex.requireModule('navbar');
            nav.setStatusBarStyle('white');
            var navigator = weex.requireModule("navigator");
            navigator.addBackGestureSelfControl();
        });

        globalEvent.addEventListener("viewWillAppear", function (e) {

            var nav = weex.requireModule('navbar');
            nav.setStatusBarStyle('white');
        });
    }
};

/***/ }),
/* 53 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      backgroundColor: "#ffffff"
    }
  }, [_c('head', {
    attrs: {
      "back": false,
      "title": "登录"
    },
    on: {
      "leftClick": _vm.leftClick
    }
  }, [_c('image', {
    staticStyle: {
      width: "80",
      height: "80"
    },
    attrs: {
      "slot": "left",
      "src": "root:img/back.png"
    },
    slot: "left"
  })]), _c('div', {
    staticStyle: {
      height: "650",
      marginTop: "120",
      marginLeft: "35",
      marginRight: "35"
    }
  }, [_c('div', {
    staticClass: ["cell"]
  }, [_c('text', {
    staticClass: ["title"]
  }, [_vm._v("账户")]), _c('input', {
    staticClass: ["input"],
    staticStyle: {
      paddingLeft: "20"
    },
    attrs: {
      "type": "text",
      "placeholder": "用户名",
      "value": (_vm.user)
    },
    on: {
      "input": function($event) {
        _vm.user = $event.target.attr.value
      }
    }
  })]), _c('div', {
    staticClass: ["cell"],
    staticStyle: {
      marginTop: "20"
    }
  }, [_c('text', {
    staticClass: ["title"]
  }, [_vm._v("密码")]), _c('input', {
    staticClass: ["input"],
    staticStyle: {
      paddingLeft: "20"
    },
    attrs: {
      "type": "password",
      "placeholder": "请输入密码",
      "value": (_vm.pass)
    },
    on: {
      "input": function($event) {
        _vm.pass = $event.target.attr.value
      }
    }
  })]), _c('button', {
    staticStyle: {
      marginTop: "120"
    },
    attrs: {
      "text": "登录",
      "disabled": _vm.btn_disabled
    },
    on: {
      "onclick": function($event) {
        _vm.login()
      }
    }
  }), _c('div', {
    staticStyle: {
      flexDirection: "row",
      justifyContent: "center",
      marginTop: "80"
    }
  }, [_c('div', {
    staticClass: ["a"],
    on: {
      "click": _vm.regist
    }
  }, [_c('text', {
    staticClass: ["text"],
    on: {
      "click": _vm.regist
    }
  }, [_vm._v("注册")])]), _c('div', {
    staticStyle: {
      flex: "1"
    }
  }), _c('text', {
    staticClass: ["text"]
  })])], 1), _c('div', {
    staticStyle: {
      flex: "1"
    }
  }), _vm._m(0)], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      marginBottom: "30",
      alignItems: "center",
      justifyContent: "center"
    }
  }, [_c('text', {
    staticStyle: {
      color: "#a2a2a2"
    }
  }, [_vm._v("授权声明：vjshop.com")])])
}]}
module.exports.render._withStripped = true

/***/ })
/******/ ]);