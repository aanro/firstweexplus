// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 79);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var host = 'http://59.110.169.246/movie/';
// var host='http://192.168.1.101:8080/'


exports.default = {

    postShort: function postShort(weg, param, header, start, success, compelete) {
        var modal = weex.requireModule("modal");
        this.postFull(weg, param, header, start, success, function (res) {
            //fail
            modal.toast({ message: res.msg });
        }, function () {
            //exception
            modal.toast({ message: '网络异常！' });
        }, function () {
            //compelete

            compelete();
        });
    },

    postFull: function postFull(weg, param, header, start, success, fail, exception, compelete) {
        var net = weex.requireModule("net");
        var modal = weex.requireModule("modal");
        var self = this;
        var url = host + weg;
        var st = weex.requireModule('static');
        var token = st.getString('token');
        if (token != undefined && token != '') {
            header.token = token;
        }
        // param.token='95d594d7b18fd1c7db37e81dd5bae9c9'
        net.post(url, param, header, function () {
            //start
            start();
        }, function (e) {
            //success
            // modal.toast({message:e.res.err})
            if (e.res.err == 0) {

                success(e.res);
            } else {
                // modal.toast({message:e.res.msg})
                if (token != undefined && token != '') {
                    st.remove('token');
                    return;
                }
                if (e.res.err == 1000) {
                    // var nav=weex.requireModule("navigator")
                    // nav.presentFull('root:busi/account/login.js',{},'transparent',true,function(){
                    //     self.postFull(weg,param,header,start,success,fail,exception,compelete);

                    // },true);
                } else fail(e.res);
            }
        }, function (e) {
            //compelete


            compelete();
        }, function (e) {
            // exception
            exception();
        });
    },

    post: function post(weg, param, success) {
        var progress = weex.requireModule("progress");
        this.postShort(weg, param, {}, function () {
            progress.show();
        }, success, function () {
            progress.dismiss();
        });
    },

    postSilent: function postSilent(weg, param, success) {

        this.postFull(weg, param, {}, function () {}, success, function (res) {
            //fail

        }, function () {
            //exception

        }, function () {
            //compelete


        });
    }

};

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = {
  "bg": {
    "backgroundColor": "#f5f5f5"
  },
  "cell": {
    "height": 100,
    "backgroundColor": "#ffffff",
    "flexDirection": "row",
    "alignItems": "center",
    "borderRadius": 5
  },
  "arrow": {
    "width": 16,
    "height": 26
  },
  "font_normal": {
    "fontSize": 30
  },
  "theme_color": {
    "color": "#1296db"
  },
  "theme_bg": {
    "color": "#1296db"
  },
  "mask": {
    "backgroundColor": "#000000",
    "opacity": 0.6,
    "position": "absolute",
    "left": 0,
    "top": 0,
    "bottom": 0,
    "right": 0
  }
}

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = {
  "layout": {
    "backgroundColor": "#333333",
    "height": 128,
    "width": 750,
    "flexDirection": "row",
    "alignItems": "center",
    "justifyContent": "center"
  }
}

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


exports.default = {
    props: {
        title: {
            default: ''

        },
        back: {
            default: true
        },
        bgcolor: {
            default: '#222222'

        },
        isloading: {
            default: false
        },
        disabled: {

            default: false
        },

        type: {
            type: String,
            default: 'text'
        },
        font_size: {
            default: 20
        },
        height: {
            default: 128
        },
        top: {
            default: 40
        },
        titletop: {
            default: 10
        }

    },
    data: function data() {
        return {};
    },

    methods: {
        titleClick: function titleClick() {
            this.$emit('titleClick');
        },
        rightclick: function rightclick() {
            this.$emit('rightClick');
        },
        backTo: function backTo() {
            if (!this.back) {
                this.$emit('leftClick');
                return;
            }

            var nav = weex.requireModule("navigator");
            nav.back();
        },
        onclick: function onclick() {
            if (!this.disabled) this.$emit('onclick');
        },
        adjust: function adjust() {
            if (weex.config.env.platform == 'android') {
                //                    if(weex.config.env.osVersion=)
                var p = weex.config.env.osVersion;
                p = p.replace(/\./g, '');
                if (p.length < 3) p = p + "0";
                if (p <= '440') {
                    this.height = 108;
                    this.top = 16;
                    this.titletop = 4;
                }
            }
        }
    },

    created: function created() {

        this.adjust();
    },
    ready: function ready() {}
    //        watch: {
    //
    //
    //            disabled:{
    //                immediate: true,
    //                handler (val) {
    //
    //                }
    //            }
    //        }
};

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["layout"],
    style: {
      'background-color': _vm.bgcolor,
      'height': _vm.height
    }
  }, [_c('div', {
    staticStyle: {
      flexDirection: "row"
    },
    style: {
      'top': _vm.titletop
    }
  }, [(_vm.isloading) ? _c('div', {
    staticStyle: {
      height: "40",
      width: "40",
      marginRight: "10"
    }
  }) : _vm._e(), _c('text', {
    staticStyle: {
      flex: "1",
      color: "#ffffff",
      textAlign: "center",
      fontSize: "38"
    },
    on: {
      "click": _vm.titleClick
    }
  }, [_vm._v(_vm._s(_vm.title))]), (_vm.isloading) ? _c('floading', {
    staticStyle: {
      height: "40",
      width: "40",
      marginLeft: "10",
      marginTop: "5"
    },
    attrs: {
      "color": "#ffffff",
      "loadingStyle": "small"
    }
  }) : _vm._e()], 1), _c('div', {
    staticStyle: {
      width: "200",
      top: "40",
      position: "absolute",
      left: "0"
    },
    style: {
      'height': _vm.height,
      'top': _vm.top
    },
    on: {
      "click": _vm.backTo
    }
  }, [(_vm.back) ? _c('image', {
    staticStyle: {
      width: "80",
      height: "80"
    },
    attrs: {
      "src": "root:img/back.png"
    }
  }) : _vm._e(), _vm._t("left")], 2), _c('div', {
    staticStyle: {
      width: "200",
      position: "absolute",
      right: "0",
      top: "0",
      alignItems: "center",
      justifyContent: "center"
    },
    style: {
      'height': _vm.height
    },
    on: {
      "click": _vm.rightclick
    }
  }, [_vm._t("right")], 2), _c('div', {
    staticStyle: {
      height: "1",
      backgroundColor: "#111111",
      position: "absolute",
      bottom: "0",
      left: "0",
      right: "0"
    }
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(1)
)
__vue_styles__.push(__webpack_require__(2)
)

/* script */
__vue_exports__ = __webpack_require__(3)

/* template */
var __vue_template__ = __webpack_require__(4)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/component/header.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-c25810c0"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = {
  "limg": {
    "width": 32,
    "height": 46
  },
  "refresh": {
    "height": 128,
    "width": 750,
    "flexDirection": "row",
    "alignItems": "center",
    "justifyContent": "center"
  },
  "refreshText": {
    "color": "#888888",
    "fontSize": 30
  },
  "indicator": {
    "color": "#888888",
    "height": 40,
    "width": 40,
    "marginRight": 10
  },
  "panel": {
    "width": "600",
    "height": "250",
    "marginLeft": "75",
    "marginTop": "35",
    "marginBottom": "35",
    "flexDirection": "column",
    "justifyContent": "center",
    "borderWidth": "2",
    "borderStyle": "solid",
    "borderColor": "#DDDDDD",
    "backgroundColor": "#F5F5F5"
  },
  "text": {
    "fontSize": "50",
    "textAlign": "center",
    "color": "#41B883"
  }
}

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


exports.default = {
    data: function data() {
        return {
            rtext: '下拉以加载',
            updatetime: '没有更新',
            offset: 0,
            deg: 20,
            refreshing: false,
            pulldistance: 135,
            hasrotate: false,
            key: "ky" + Math.random()
        };
    },

    methods: {
        animateArrow: function animateArrow(deg) {
            var animation = weex.requireModule('animation');
            var arrow = this.$refs.arrow;
            //                var deg=this.hasrotate?180:0

            animation.transition(arrow, {
                styles: {
                    transform: "rotate(" + deg + "deg" + ")"
                },

                duration: 150, //ms
                timingFunction: 'ease',
                delay: 0 //ms
            }, function () {});
        },
        onrefresh: function onrefresh(event) {
            if (this.offset >= this.pulldistance) {

                this.refreshing = true;
                this.rtext = "加载中";
                this.$emit('onRefresh');
                //                    setTimeout(() => {
                //                        this.refreshing = false
                //                    }, 2000)
            }
        },
        end: function end() {
            this.refreshing = false;
            //                this.deg=0;
            this.updatetime = this.getNowFormatDate();
            //                this.rtext='下拉以加载'
        },
        onpullingdown: function onpullingdown(event) {

            var dis = event.pullingDistance;
            if (dis < 0) dis *= -1;
            this.offset = dis;

            if (this.refreshing == false) {

                //                     var t=dis>this.pulldistance
                //                    if(t!=this.hasrotate)
                //                    {
                //                        this.hasrotate=t;
                //                        this.animateArrow();
                //                    }
                if (dis > this.pulldistance) {
                    this.rtext = "松开刷新";
                    this.deg = 180;
                    this.hasrotate = false;
                    this.animateArrow(180);
                } else {
                    var p = dis / this.pulldistance;
                    if (p > 1) p == 1;
                    this.deg = p * 180;
                    this.animateArrow(0);
                    this.rtext = '下拉以加载';
                }
            }
        },
        getNowFormatDate: function getNowFormatDate() {
            var date = new Date();
            var seperator1 = "-";
            var seperator2 = ":";
            var month = date.getMonth() + 1;
            var strDate = date.getDate();
            var min = date.getMinutes();
            var secon = date.getSeconds();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            if (min >= 0 && min <= 9) {
                min = "0" + min;
            }
            if (secon >= 0 && secon <= 9) {
                secon = "0" + secon;
            }

            var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate + " " + date.getHours() + seperator2 + min + seperator2 + secon;
            return currentdate;
        }
    },

    created: function created() {}
};

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('refresh', {
    key: _vm.key,
    staticClass: ["refresh"],
    attrs: {
      "id": "rex",
      "display": _vm.refreshing ? 'show' : 'hide'
    },
    on: {
      "refresh": _vm.onrefresh,
      "pullingdown": _vm.onpullingdown
    }
  }, [_c('div', {
    staticStyle: {
      flexDirection: "row"
    }
  }, [(_vm.refreshing) ? _c('floading', {
    staticClass: ["indicator"],
    attrs: {
      "color": "#555555"
    }
  }) : _vm._e(), (!_vm.refreshing) ? _c('image', {
    ref: "arrow",
    staticClass: ["limg"],
    attrs: {
      "src": "root:img/pull_arrow.png"
    }
  }) : _vm._e(), _c('div', {
    staticStyle: {
      alignItems: "center"
    }
  }, [_c('text', {
    staticClass: ["refreshText"]
  }, [_vm._v(_vm._s(_vm.rtext))]), _c('text', {
    staticStyle: {
      fontSize: "25",
      color: "#888888"
    }
  }, [_vm._v("上次更新:" + _vm._s(_vm.updatetime))])])], 1)])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = {
  "progress": {
    "width": 220,
    "height": 220,
    "opacity": 0.8,
    "backgroundColor": "#000000",
    "borderRadius": 30,
    "justifyContent": "center",
    "alignItems": "center",
    "position": "absolute",
    "left": 265,
    "top": 300
  },
  "exception": {
    "marginTop": 50,
    "borderRadius": 10,
    "justifyContent": "center",
    "alignItems": "center",
    "width": 300,
    "height": 80,
    "borderWidth": 2,
    "borderColor": "#949494",
    "backgroundColor": "#ffffff",
    "backgroundColor:active": "#dddddd"
  },
  "emptytxt": {
    "marginTop": 100,
    "color": "#999999"
  },
  "empty": {
    "height": 500,
    "alignItems": "center"
  },
  "loading": {
    "height": 90,
    "justifyContent": "center",
    "alignItems": "center",
    "flexDirection": "row",
    "borderRadius": 5,
    "borderWidth": 1,
    "borderColor": "#e6e6e6",
    "backgroundColor": "#fdfdfd",
    "marginTop": 15,
    "marginRight": 30,
    "marginBottom": 15,
    "marginLeft": 30
  },
  "c": {
    "flex": 1
  },
  "page": {
    "backgroundColor": "#ffffff",
    "width": 750,
    "flex": 1
  }
}

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var pull = __webpack_require__(11);
var net = __webpack_require__(0);

exports.default = {
    components: { pull: pull },
    props: {
        columnCount: {
            default: 1
        },
        key: {
            default: 'key1'
        },
        columnGap: {
            default: 20
        },
        columnWidth: {
            default: 'auto'
        },
        scrollable: {
            default: true
        },
        showScrollbar: {
            default: true
        },
        showheader: {
            default: false
        },
        usePullRefresh: {
            default: true
        },
        items: {
            default: []
        },
        isEmpty: {
            default: false
        },
        background: {
            default: '#ffffff'
        },
        isException: {
            default: false
        },
        pageSize: {
            default: 15
        },
        isloading: {
            default: false
        },
        loadOnInit: {
            default: true
        },
        hasmore: {
            default: true
        },
        emptyTxt: {
            default: '您的订单为空~'
        },
        img_empty_src: {
            default: 'root:img/ico_empty.png'
        },

        img_exception_src: {
            default: 'root:img/ico_exception.png'
        },
        showweg: {
            default: false
        },
        ispull: {
            default: false
        }

    },
    data: function data() {

        return {
            pullkey: this.key + "pull",
            waterkey: this.key + "water",
            _columnCount: this.getCount()

        };
    },
    mounted: function mounted() {

        if (this.loadOnInit) {
            var self = this;
            self.$emit('load');
        }
    },


    methods: {

        loadmore: function loadmore(e) {

            if (this.hasmore) this.$emit('load');
        },
        getCount: function getCount() {
            if (this.isEmpty || this.isException) {
                return 1;
            } else {
                return this.columnCount;
            }
        },

        clear: function clear() {

            this.hasmore = true;
            this.items.length = 0;
        },
        showEmpty: function showEmpty() {
            this._columnCount = this.getCount();
            this.isEmpty = true;
            this.isException = false;
        },
        showException: function showException() {
            this._columnCount = this.getCount();
            this.isEmpty = false;
            this.isException = true;
        },
        load: function load(url, param, items, callback) {
            this.loadFull(url, param, items, callback, function () {}, function () {}, function () {}, function () {});
        },
        loadFull: function loadFull(url, param, items, callback, start, fail, exp, comp) {
            //                var progress=weex.requireModule("progress")
            if (this.isloading) return;
            this.isException = false;
            this.isEmpty = false;
            this.isloading = true;

            var self = this;
            //                postFull:  function (weg,param,header,start,success,compelete)

            var modal = weex.requireModule('modal');
            net.postFull(url, param, {}, function () {
                //start
                start();
            }, function (res) {
                //success
                if (res.list.length < 15) {
                    self.hasmore = false;
                }
                if (res.list.length > 0) {
                    items = items.concat(res.list);
                    self.items = items;
                }
                if (items.length == 0) {
                    self.showEmpty();
                }
                callback(items);
                self._columnCount = self.getCount();
            }, function (e) {
                //fail
                fail();
                modal.toast({ message: e.res.error });
            }, function () {
                //exception
                exp();
                if (items.length == 0) {
                    self.showException();
                } else {
                    modal.toast({ message: '网络异常' });
                }
            }, function (e) {
                //compelete
                //                    progress.dismiss();
                self.isloading = false;
                self.$refs.refresh.end();
                comp();
            });
        },

        hideRefresh: function hideRefresh() {
            var p = this.$refs.refresh;
            p.end();
            this.ispull = false;
        },
        refresh: function refresh() {
            this.isEmpty = false;
            this.isException = false;
            this.isloading = false;
            this.ispull = true;
            this.$emit('refresh');
        },
        reload: function reload() {
            this.isEmpty = false;
            this.isException = false;
            this.isloading = false;
            this.ispull = false;
            this.$emit('refresh');
        }

    }
};

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(6)
)

/* script */
__vue_exports__ = __webpack_require__(7)

/* template */
var __vue_template__ = __webpack_require__(8)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/component/pullrefresh.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-6ba27873"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 12 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["c"]
  }, [_c('recycleList', {
    key: _vm.waterkey,
    staticClass: ["page"],
    style: {
      'background-color': _vm.background
    },
    attrs: {
      "columnCount": _vm.getCount(),
      "columnGap": _vm.columnGap,
      "showScrollbar": _vm.showScrollbar,
      "scrollable": _vm.scrollable,
      "loadmoreoffset": "141"
    },
    on: {
      "loadmore": _vm.loadmore
    }
  }, [(_vm.usePullRefresh) ? _c('pull', {
    ref: "refresh",
    on: {
      "onRefresh": _vm.refresh
    }
  }) : _vm._e(), _c('header', {
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('div', [_vm._t("head")], 2)]), _vm._t("cell"), _c('cell', {
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('div', [_vm._t("foot")], 2)]), (_vm.isEmpty) ? _c('cell', {
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('div', {
    staticClass: ["empty"]
  }, [_c('image', {
    staticStyle: {
      width: "252",
      height: "201",
      marginTop: "100"
    },
    attrs: {
      "src": _vm.img_empty_src
    }
  }), _c('text', {
    staticClass: ["emptytxt"],
    staticStyle: {
      marginTop: "20"
    }
  }, [_vm._v(_vm._s(_vm.emptyTxt))])])]) : _vm._e(), (_vm.isException) ? _c('cell', {
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('div', {
    staticClass: ["empty"]
  }, [_c('image', {
    staticStyle: {
      width: "252",
      height: "201",
      marginTop: "100"
    },
    attrs: {
      "src": _vm.img_exception_src
    }
  }), _c('text', {
    staticStyle: {
      color: "#000000",
      marginTop: "20"
    }
  }, [_vm._v("网络请求失败")]), _c('div', {
    staticClass: ["exception"],
    on: {
      "click": _vm.reload
    }
  }, [_c('text', {
    staticStyle: {
      color: "#000000",
      fontSize: "28"
    }
  }, [_vm._v("重新加载")])])])]) : _vm._e(), (!_vm.ispull && _vm.isloading && _vm.items.length > 0) ? _c('cell', {
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('div', {
    staticClass: ["loading"]
  }, [_c('floading', {
    staticStyle: {
      height: "40",
      width: "40"
    },
    attrs: {
      "color": "#999999",
      "loadingStyle": "small"
    }
  }), _c('text', {
    staticStyle: {
      marginLeft: "10",
      color: "#999999",
      fontSize: "28"
    }
  }, [_vm._v("正在载入")])], 1)]) : _vm._e()], 2), (!_vm.ispull && _vm.isloading && _vm.items.length == 0) ? _c('div', {
    staticClass: ["progress"]
  }, [_c('floading', {
    staticStyle: {
      height: "40",
      width: "40",
      marginTop: "20"
    },
    attrs: {
      "color": "#ffffff",
      "loadingStyle": "big"
    }
  }), _c('text', {
    staticStyle: {
      marginLeft: "0",
      color: "#ffffff",
      fontSize: "30",
      marginTop: "30",
      fontWeight: "bold"
    }
  }, [_vm._v("加载中...")])], 1) : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(9)
)

/* script */
__vue_exports__ = __webpack_require__(10)

/* template */
var __vue_template__ = __webpack_require__(12)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/component/flist.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-01a22241"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {

    login: function login(user, pass, comp) {

        var net = __webpack_require__(0);
        net.post('login.do', { name: user, pass: pass }, function (e) {

            var st = weex.requireModule('static');

            st.set('user', e.user);

            st.setString('token', e.user.token);
            comp(e);
        });
    },
    checkDo: function checkDo(success) {
        var navigator = weex.requireModule('navigator');
        navigator.present('root:busi/account/login.js', {}, 'transparent', true, function () {
            success();
        }, true);
    },
    isLogin: function isLogin() {
        var st = weex.requireModule('static');
        var usr = st.get('user');
        // var modal=weex.requireModule('modal');
        // modal.toast({message:usr})
        if (usr != undefined && usr != '') return true;
        return false;
    }

};

/***/ }),
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */
/***/ (function(module, exports) {

module.exports = {
  "bg": {
    "backgroundColor": "#f5f5f5"
  },
  "cell": {
    "height": 100,
    "backgroundColor": "#ffffff",
    "flexDirection": "row",
    "alignItems": "center",
    "borderRadius": 5
  },
  "arrow": {
    "width": 16,
    "height": 26
  },
  "font_normal": {
    "fontSize": 30
  },
  "theme_color": {
    "color": "#1296db"
  },
  "theme_bg": {
    "color": "#1296db"
  },
  "mask": {
    "backgroundColor": "#000000",
    "opacity": 0.6,
    "position": "absolute",
    "left": 0,
    "top": 0,
    "bottom": 0,
    "right": 0
  }
}

/***/ }),
/* 24 */
/***/ (function(module, exports) {

module.exports = {
  "item": {
    "flex": 1,
    "alignItems": "center",
    "justifyContent": "center",
    "height": 55,
    "borderRightWidth": 3,
    "borderColor": "#1296db",
    "fontSize": 30
  },
  "item_sel": {
    "flex": 1,
    "alignItems": "center",
    "justifyContent": "center",
    "height": 55,
    "backgroundColor": "#1296db",
    "fontSize": 30
  }
}

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


exports.default = {
    props: {
        theme: {
            default: 'red'
        },
        items: {
            default: ['剧集', '简介', '简介']

        },
        selectIndex: {
            default: 0
        }

    },
    data: function data() {
        return {

            visiable: true

        };
    },

    methods: {
        onitemclick: function onitemclick(index) {
            this.selectIndex = index;
            this.$emit('change', index);
        },
        getBorderColor: function getBorderColor(index) {
            //                return this.theme
            if (this.items.length - 1 != index) {
                return this.theme;
            } else {
                return 'transparent';
            }
        },
        getBg: function getBg(index) {
            if (this.selectIndex == index) {
                return this.theme;
            } else {
                return 'transparent';
            }
        }
    },

    created: function created() {

        this.visiable = !this.value == '';
    },
    ready: function ready() {}
    //        watch: {
    //
    //
    //            disabled:{
    //                immediate: true,
    //                handler (val) {
    //
    //                }
    //            }
    //        }
};

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      borderRadius: "10",
      borderWidth: "3",
      flexDirection: "row"
    },
    style: {
      'border-color': _vm.theme
    }
  }, _vm._l((_vm.items), function(item, index) {
    return _c('div', {
      class: [_vm.selectIndex == index ? 'item_sel' : 'item'],
      style: {
        'border-right-width': index == _vm.items.length - 1 ? 0 : 3,
        'background-color': _vm.getBg(index),
        'border-color': _vm.getBorderColor(index)
      },
      on: {
        "click": function($event) {
          _vm.onitemclick(index)
        }
      }
    }, [_c('text', {
      staticStyle: {
        color: "#ffffff",
        fontSize: "30"
      }
    }, [_vm._v(_vm._s(item))])])
  }))
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */
/***/ (function(module, exports) {

module.exports = {
  "bg": {
    "backgroundColor": "#f5f5f5"
  },
  "cell": {
    "height": 100,
    "backgroundColor": "#ffffff",
    "flexDirection": "row",
    "alignItems": "center",
    "borderRadius": 5
  },
  "arrow": {
    "width": 16,
    "height": 26
  },
  "font_normal": {
    "fontSize": 30
  },
  "theme_color": {
    "color": "#1296db"
  },
  "theme_bg": {
    "color": "#1296db"
  },
  "mask": {
    "backgroundColor": "#000000",
    "opacity": 0.6,
    "position": "absolute",
    "left": 0,
    "top": 0,
    "bottom": 0,
    "right": 0
  }
}

/***/ }),
/* 32 */
/***/ (function(module, exports) {

module.exports = {
  "text": {
    "color": "#ffffff",
    "fontSize": 30
  },
  "text-disabled": {
    "color": "#b4b4b4",
    "fontSize": 30
  },
  "button": {
    "height": 100,
    "backgroundColor": "#ff4800",
    "alignItems": "center",
    "justifyContent": "center",
    "color": "#ffffff",
    "borderRadius": 8,
    "backgroundColor:active": "#ff1b08"
  },
  "button-disabled": {
    "height": 100,
    "backgroundColor": "#eeeeee",
    "alignItems": "center",
    "justifyContent": "center",
    "color": "#ffffff",
    "borderRadius": 8,
    "backgroundColor:active": "#eeeeee"
  }
}

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//


exports.default = {
    props: {
        text: {
            type: String

        },
        color: {
            type: String

        },
        disabled: {

            default: false
        },

        type: {
            type: String,
            default: 'text'
        },
        font_size: {
            default: 20
        }

    },
    data: function data() {
        return {

            visiable: true

        };
    },

    methods: {
        oninput: function oninput(e) {

            //                this.$emit('oninput');
            this.$emit('oninput', e);
            this.visiable = e.value != '';
        },
        onclick: function onclick() {
            if (!this.disabled) this.$emit('onclick');
        },
        panstart: function panstart() {
            if (!this.disabled) this.bgcolor = '#ff1b08';
        },
        panend: function panend() {
            if (!this.disabled) this.bgcolor = '#ff4800';
        },
        setenable: function setenable() {},
        onclose: function onclose() {
            this.value = '';
        }
    },

    created: function created() {

        this.visiable = !this.value == '';
    },
    ready: function ready() {}
    //        watch: {
    //
    //
    //            disabled:{
    //                immediate: true,
    //                handler (val) {
    //
    //                }
    //            }
    //        }
};

/***/ }),
/* 34 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      flexDirection: "row"
    }
  }, _vm._l(([1, 1, 1, 1, 1]), function(item) {
    return _c('image', {
      staticStyle: {
        width: "42",
        height: "42",
        marginLeft: "5"
      },
      attrs: {
        "src": "root:img/star.png"
      }
    })
  }))
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(31)
)
__vue_styles__.push(__webpack_require__(32)
)

/* script */
__vue_exports__ = __webpack_require__(33)

/* template */
var __vue_template__ = __webpack_require__(34)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/component/star.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-3ab1b3a5"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(23)
)
__vue_styles__.push(__webpack_require__(24)
)

/* script */
__vue_exports__ = __webpack_require__(25)

/* template */
var __vue_template__ = __webpack_require__(26)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/component/indicator.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-11007a4c"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */,
/* 55 */,
/* 56 */,
/* 57 */,
/* 58 */,
/* 59 */,
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(80)
)
__vue_styles__.push(__webpack_require__(81)
)

/* script */
__vue_exports__ = __webpack_require__(82)

/* template */
var __vue_template__ = __webpack_require__(83)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/moviedetail.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-57e66b50"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),
/* 80 */
/***/ (function(module, exports) {

module.exports = {
  "bg": {
    "backgroundColor": "#f5f5f5"
  },
  "cell": {
    "height": 100,
    "backgroundColor": "#ffffff",
    "flexDirection": "row",
    "alignItems": "center",
    "borderRadius": 5
  },
  "arrow": {
    "width": 16,
    "height": 26
  },
  "font_normal": {
    "fontSize": 30
  },
  "theme_color": {
    "color": "#1296db"
  },
  "theme_bg": {
    "color": "#1296db"
  },
  "mask": {
    "backgroundColor": "#000000",
    "opacity": 0.6,
    "position": "absolute",
    "left": 0,
    "top": 0,
    "bottom": 0,
    "right": 0
  }
}

/***/ }),
/* 81 */
/***/ (function(module, exports) {

module.exports = {
  "progress": {
    "width": 220,
    "height": 220,
    "opacity": 0.8,
    "backgroundColor": "#000000",
    "borderRadius": 30,
    "justifyContent": "center",
    "alignItems": "center",
    "position": "absolute",
    "left": 265,
    "top": 650
  },
  "dl": {
    "flexDirection": "row",
    "marginTop": 15
  },
  "indicator": {
    "width": "750",
    "height": "316",
    "position": "absolute",
    "itemColor": "#ffffff",
    "itemSelectedColor": "#777777",
    "itemSize": "20",
    "top": "120",
    "left": 1
  },
  "slider": {
    "width": "750",
    "height": "316"
  },
  "frame": {
    "width": "750",
    "height": "316",
    "position": "relative"
  },
  "image": {
    "width": "750",
    "height": "316"
  }
}

/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var flist = __webpack_require__(13);
var head = __webpack_require__(5);
var star = __webpack_require__(46);
var cator = __webpack_require__(47);
var net = __webpack_require__(0);
var login = __webpack_require__(14);
exports.default = {
    components: { flist: flist, head: head, star: star, cator: cator },
    data: function data() {
        return {

            item: {},
            selectIndex: 0,
            isloading: false,
            showmask: true,
            coltext: '收藏'

        };
    },

    methods: {
        collect: function collect() {
            var _this = this;

            if (this.coltext == "已收藏") {
                return;
            }
            var p = {};
            p.filmid = this.item.id;
            net.post('auth/collect/add.do', p, function (res) {
                var notify = weex.requireModule("notify");
                notify.send('collectionrefrsh', {});
                _this.coltext = "已收藏";
                var modal = weex.requireModule("modal");
                modal.toast({ message: '收藏成功!' });
            });
        },
        check: function check() {
            var _this2 = this;

            if (!login.isLogin()) return;
            var p = {};
            p.filmid = this.item.id;
            net.postSilent('auth/collect/check.do', p, function (res) {
                if (res.collect) {
                    _this2.coltext = "已收藏";
                } else _this2.coltext = "收藏";
            });
        },
        copy: function copy() {
            var clipboard = weex.requireModule('clipboard');
            clipboard.setString(this.item.download_url);
            var modal = weex.requireModule('modal');
            //                modal.toast({message:this.item.download_url})
            modal.toast({ message: '复制成功' });
        },
        getVisable: function getVisable(it) {
            if (it.serial == undefined) return false;
            return it.serial.length == 0;
        },
        change: function change(index) {
            this.selectIndex = index;
        },
        load: function load(id) {
            var _this3 = this;

            net.postShort('movieById.do', _defineProperty({ id: id }, 'id', id), {}, function () {
                //start
                _this3.isloading = true;
            }, function (res) {
                _this3.item = res.film;
            }, function () {
                _this3.isloading = false;
                if (_this3.item.des != '') _this3.showmask = false;
            });
        }
    },

    created: function created() {

        var self = this;
        var globalEvent = weex.requireModule('globalEvent');
        globalEvent.addEventListener("onPageInit", function (e) {

            var notify = weex.requireModule("notify");
            notify.regist('login', function () {
                self.check();
            });
            var nav = weex.requireModule("navigator");
            self.item = nav.param();
            self.load(self.item.id);
            if (!login.isLogin()) return;
            self.check();
        });
    }

    //        watch: {
    //
    //
    //            disabled:{
    //                immediate: true,
    //                handler (val) {
    //
    //                }
    //            }
    //        }
};

/***/ }),
/* 83 */
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      backgroundColor: "#333333"
    },
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('head', {
    attrs: {
      "title": "详情"
    },
    on: {
      "rightClick": _vm.collect
    }
  }, [_c('text', {
    staticStyle: {
      marginTop: "25",
      color: "#ffffff",
      marginLeft: "80",
      fontSize: "30"
    },
    style: {
      'margin-left': _vm.coltext == '收藏' ? 80 : 50
    },
    attrs: {
      "slot": "right"
    },
    slot: "right"
  }, [_vm._v(_vm._s(_vm.coltext))])]), _c('div', {
    staticStyle: {
      flexDirection: "row"
    }
  }, [_c('image', {
    staticStyle: {
      width: "220",
      height: "330",
      marginLeft: "40",
      marginTop: "40"
    },
    attrs: {
      "resize": "cover",
      "placeholder": "root:img/default.png",
      "src": _vm.item.img
    }
  }), _c('div', {
    staticStyle: {
      flex: "1",
      height: "330",
      marginTop: "40",
      paddingLeft: "20"
    }
  }, [_c('div', {
    staticStyle: {
      flexDirection: "row"
    }
  }, [_c('text', {
    staticStyle: {
      color: "#ffffff",
      fontSize: "35",
      fontWeight: "bold",
      line: "2",
      width: "400"
    }
  }, [_vm._v(_vm._s(_vm.item.name) + _vm._s(_vm.item.id))])]), _c('div', {
    staticClass: ["dl"]
  }, [_c('text', {
    staticStyle: {
      color: "#ffffff",
      fontSize: "30"
    }
  }, [_vm._v("类型:")]), _c('text', {
    staticStyle: {
      color: "#ffffff",
      fontSize: "30"
    }
  }, [_vm._v("  " + _vm._s(_vm.item.typeName))])]), _c('div', {
    staticClass: ["dl"]
  }, [_c('text', {
    staticStyle: {
      color: "#ffffff",
      fontSize: "30"
    }
  }, [_vm._v("地区:")]), _c('text', {
    staticStyle: {
      color: "#ffffff",
      fontSize: "30"
    }
  }, [_vm._v("  " + _vm._s(_vm.item.country))])]), _c('div', {
    staticClass: ["dl"]
  }, [_c('text', {
    staticStyle: {
      color: "#ffffff",
      fontSize: "30"
    }
  }, [_vm._v("年代:")]), _c('text', {
    staticStyle: {
      color: "#ffffff",
      fontSize: "30",
      marginTop: "5"
    }
  }, [_vm._v("  " + _vm._s(_vm.item.year))])])])]), _c('div', {
    staticStyle: {
      flex: "1",
      marginTop: "50"
    }
  }, [_c('div', {
    staticStyle: {
      position: "absolute",
      left: "20",
      top: "10",
      right: "20",
      bottom: "100"
    }
  }, [_c('web', {
    staticStyle: {
      backgroundColor: "#333333",
      position: "absolute",
      left: "0",
      right: "0",
      top: "0",
      bottom: "0"
    },
    attrs: {
      "src": _vm.item.des
    }
  }), _c('div', {
    staticStyle: {
      position: "absolute",
      left: "0",
      top: "0",
      right: "0",
      bottom: "0",
      backgroundColor: "#333333"
    },
    style: {
      'visibility': _vm.showmask ? 'visible' : 'hidden'
    }
  })])]), (_vm.isloading) ? _c('div', {
    staticClass: ["progress"]
  }, [_c('floading', {
    staticStyle: {
      height: "40",
      width: "40",
      marginTop: "20"
    },
    attrs: {
      "color": "#ffffff",
      "loadingStyle": "big"
    }
  }), _c('text', {
    staticStyle: {
      marginLeft: "0",
      color: "#ffffff",
      fontSize: "30",
      marginTop: "30",
      fontWeight: "bold"
    }
  }, [_vm._v("加载中...")])], 1) : _vm._e(), _c('div', {
    staticStyle: {
      height: "100",
      position: "absolute",
      bottom: "0",
      left: "0",
      right: "0",
      backgroundColor: "#0088fb",
      alignItems: "center",
      justifyContent: "center"
    },
    on: {
      "click": _vm.copy
    }
  }, [_c('text', {
    staticStyle: {
      color: "#ffffff",
      fontSize: "32"
    }
  }, [_vm._v("点击下载")])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ })
/******/ ]);