// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 74);
/******/ })
/************************************************************************/
/******/ ({

/***/ 42:
/***/ (function(module, exports) {

module.exports = {
  "bg": {
    "backgroundColor": "#f5f5f5"
  },
  "cell": {
    "height": 100,
    "backgroundColor": "#ffffff",
    "flexDirection": "row",
    "alignItems": "center",
    "borderRadius": 5
  },
  "arrow": {
    "width": 16,
    "height": 26
  },
  "font_normal": {
    "fontSize": 30
  },
  "theme_color": {
    "color": "#1296db"
  },
  "theme_bg": {
    "color": "#1296db"
  },
  "mask": {
    "backgroundColor": "#000000",
    "opacity": 0.6,
    "position": "absolute",
    "left": 0,
    "top": 0,
    "bottom": 0,
    "right": 0
  }
}

/***/ }),

/***/ 43:
/***/ (function(module, exports) {

module.exports = {
  "text": {
    "color": "#ffffff",
    "fontSize": 30
  },
  "text-disabled": {
    "color": "#b4b4b4",
    "fontSize": 30
  },
  "button": {
    "height": 100,
    "backgroundColor": "#ff4800",
    "alignItems": "center",
    "justifyContent": "center",
    "color": "#ffffff",
    "borderRadius": 8,
    "backgroundColor:active": "#ff1b08"
  },
  "button-disabled": {
    "height": 100,
    "backgroundColor": "#eeeeee",
    "alignItems": "center",
    "justifyContent": "center",
    "color": "#ffffff",
    "borderRadius": 8,
    "backgroundColor:active": "#eeeeee"
  }
}

/***/ }),

/***/ 44:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//


exports.default = {
    props: {

        text: {
            default: ''

        },
        currentIndex: {
            default: 0
        },
        color: {
            default: '#000000'
        },
        selectColor: {
            default: '#000000'
        },
        img: {
            default: ''
        },
        selectImg: {
            default: ''
        },
        select: {
            default: false
        },

        fontSize: {
            default: 28
        },
        index: {
            default: 0
        }

    },
    data: function data() {
        return {

            visiable: true

        };
    },

    methods: {
        layoutclick: function layoutclick() {
            this.select = !this.select;
            this.$emit('change', this.index);
        },
        oninput: function oninput(e) {

            //                this.$emit('oninput');
            this.$emit('oninput', e);
            this.visiable = e.value != '';
        },
        onclick: function onclick() {
            if (!this.disabled) this.$emit('onclick');
        },
        panstart: function panstart() {
            if (!this.disabled) this.bgcolor = '#ff1b08';
        },
        panend: function panend() {
            if (!this.disabled) this.bgcolor = '#ff4800';
        },
        setenable: function setenable() {},
        onclose: function onclose() {
            this.value = '';
        }
    },

    created: function created() {

        this.visiable = !this.value == '';
    },
    ready: function ready() {}
    //        watch: {
    //
    //
    //            disabled:{
    //                immediate: true,
    //                handler (val) {
    //
    //                }
    //            }
    //        }
};

/***/ }),

/***/ 45:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      alignItems: "center",
      justifyContent: "center"
    },
    on: {
      "click": _vm.layoutclick
    }
  }, [_c('image', {
    staticStyle: {
      width: "50",
      height: "50",
      marginTop: "10"
    },
    attrs: {
      "src": _vm.currentIndex == _vm.index ? _vm.selectImg : _vm.img
    }
  }), _c('text', {
    staticStyle: {
      marginTop: "5"
    },
    style: {
      'font-size': _vm.fontSize,
      'color': _vm.currentIndex == _vm.index ? _vm.selectColor : _vm.color
    }
  }, [_vm._v(_vm._s(_vm.text))])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 74:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(75)
)

/* script */
__vue_exports__ = __webpack_require__(76)

/* template */
var __vue_template__ = __webpack_require__(78)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/index.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-2f06b53e"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 75:
/***/ (function(module, exports) {

module.exports = {
  "title": {
    "paddingTop": "40",
    "paddingBottom": "40",
    "fontSize": "48"
  },
  "logo": {
    "width": "360",
    "height": "156"
  },
  "desc": {
    "paddingTop": "20",
    "color": "#888888",
    "fontSize": "24"
  },
  "tabitem": {
    "flex": 1
  }
}

/***/ }),

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var tabitem = __webpack_require__(77);
exports.default = {
    components: { tabitem: tabitem },
    data: {
        logoUrl: 'http://img1.vued.vanthink.cn/vued08aa73a9ab65dcbd360ec54659ada97c.png',
        viewadd: false,
        index: 0,
        items: ['root:busi/tab/mainpage.js', 'root:busi/tab/serial.js', 'root:busi/tab/movie.js', 'root:busi/tab/collection.js']
    },

    methods: {
        update: function update(e) {
            this.target = 'Weex';
            console.log('target:', this.target);
        },
        change: function change(i) {
            if (i == 3) {
                var st = weex.requireModule("static");
                var usr = st.get('user');
                if (usr == undefined || usr == '') {
                    var nav = weex.requireModule("navigator");
                    nav.present('account/login.js');
                    return;
                }
            }
            this.index = i;
        },
        firstviewadd: function firstviewadd() {
            this.viewadd = true;
        },
        show: function show() {
            var modal = weex.requireModule("modal");
            var p = weex.config.env.osVersion;
            //              p=p.replace('.', "")
            //          p=p.replace(/./g,"");
            p = p.replace(/\./g, '');
            modal.alert({ message: p });
        }
    },

    created: function created() {
        var _this = this;

        var notify = weex.requireModule("notify");
        notify.regist('tabselect', function (e) {

            _this.index = e.index;
        });

        var globalEvent = weex.requireModule('globalEvent');
        var self = this;
        globalEvent.addEventListener("onPageInit", function (e) {

            var page = weex.requireModule("pagemodule");
            page.doubleBack();

            var navbar = weex.requireModule("navbar");
            navbar.setStatusBarStyle('white');
            var nav = weex.requireModule("navigator");
            nav.addBackGestureSelfControl();
        });

        globalEvent.addEventListener("viewWillAppear", function (e) {

            var nav = weex.requireModule('navbar');

            nav.setStatusBarStyle('white');
            //              nav.setBackgroundColor("#ffffff");
            //              nav.makeTransparent();
        });
    }
};

/***/ }),

/***/ 77:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(42)
)
__vue_styles__.push(__webpack_require__(43)
)

/* script */
__vue_exports__ = __webpack_require__(44)

/* template */
var __vue_template__ = __webpack_require__(45)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/component/tabitem.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-078d7525"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 78:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      flex: "1"
    }
  }, [_c('host', {
    staticStyle: {
      position: "absolute",
      left: "0",
      top: "0",
      right: "0",
      bottom: "100"
    },
    attrs: {
      "index": _vm.index,
      "items": _vm.items
    },
    on: {
      "firstviewadd": _vm.firstviewadd
    }
  }), _c('div', {
    staticStyle: {
      height: "110",
      width: "750",
      position: "absolute",
      bottom: "0",
      left: "0",
      right: "0",
      flexDirection: "row",
      backgroundColor: "#1c1b1b"
    }
  }, [_c('tabitem', {
    staticClass: ["tabitem"],
    attrs: {
      "index": 0,
      "currentIndex": _vm.index,
      "text": "推荐",
      "img": "root:img/tab1_unselect.png",
      "selectImg": "root:img/tab1_select.png",
      "color": "#666666",
      "selectColor": "#1296db"
    },
    on: {
      "change": _vm.change
    }
  }), _c('tabitem', {
    staticClass: ["tabitem"],
    attrs: {
      "index": 1,
      "currentIndex": _vm.index,
      "text": "电视剧",
      "img": "root:img/tab2_unselect.png",
      "selectImg": "root:img/tab2_select.png",
      "color": "#666666",
      "selectColor": "#1296db"
    },
    on: {
      "change": _vm.change
    }
  }), _c('tabitem', {
    staticClass: ["tabitem"],
    attrs: {
      "index": 2,
      "currentIndex": _vm.index,
      "text": "电影",
      "img": "root:img/tab3_unselect.png",
      "selectImg": "root:img/tab3_select.png",
      "color": "#666666",
      "selectColor": "#1296db"
    },
    on: {
      "change": _vm.change
    }
  }), _c('tabitem', {
    staticClass: ["tabitem"],
    attrs: {
      "index": 3,
      "currentIndex": _vm.index,
      "text": "收藏",
      "img": "root:img/tab4_unselect.png",
      "selectImg": "root:img/tab4_select.png",
      "color": "#666666",
      "selectColor": "#1296db"
    },
    on: {
      "change": _vm.change
    }
  })], 1)], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ })

/******/ });