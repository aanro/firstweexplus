// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 95);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var host = 'http://59.110.169.246/movie/';
// var host='http://192.168.1.101:8080/'


exports.default = {

    postShort: function postShort(weg, param, header, start, success, compelete) {
        var modal = weex.requireModule("modal");
        this.postFull(weg, param, header, start, success, function (res) {
            //fail
            modal.toast({ message: res.msg });
        }, function () {
            //exception
            modal.toast({ message: '网络异常！' });
        }, function () {
            //compelete

            compelete();
        });
    },

    postFull: function postFull(weg, param, header, start, success, fail, exception, compelete) {
        var net = weex.requireModule("net");
        var modal = weex.requireModule("modal");
        var self = this;
        var url = host + weg;
        var st = weex.requireModule('static');
        var token = st.getString('token');
        if (token != undefined && token != '') {
            header.token = token;
        }
        // param.token='95d594d7b18fd1c7db37e81dd5bae9c9'
        net.post(url, param, header, function () {
            //start
            start();
        }, function (e) {
            //success
            // modal.toast({message:e.res.err})
            if (e.res.err == 0) {

                success(e.res);
            } else {
                // modal.toast({message:e.res.msg})
                if (token != undefined && token != '') {
                    st.remove('token');
                    return;
                }
                if (e.res.err == 1000) {
                    // var nav=weex.requireModule("navigator")
                    // nav.presentFull('root:busi/account/login.js',{},'transparent',true,function(){
                    //     self.postFull(weg,param,header,start,success,fail,exception,compelete);

                    // },true);
                } else fail(e.res);
            }
        }, function (e) {
            //compelete


            compelete();
        }, function (e) {
            // exception
            exception();
        });
    },

    post: function post(weg, param, success) {
        var progress = weex.requireModule("progress");
        this.postShort(weg, param, {}, function () {
            progress.show();
        }, success, function () {
            progress.dismiss();
        });
    },

    postSilent: function postSilent(weg, param, success) {

        this.postFull(weg, param, {}, function () {}, success, function (res) {
            //fail

        }, function () {
            //exception

        }, function () {
            //compelete


        });
    }

};

/***/ }),

/***/ 1:
/***/ (function(module, exports) {

module.exports = {
  "bg": {
    "backgroundColor": "#f5f5f5"
  },
  "cell": {
    "height": 100,
    "backgroundColor": "#ffffff",
    "flexDirection": "row",
    "alignItems": "center",
    "borderRadius": 5
  },
  "arrow": {
    "width": 16,
    "height": 26
  },
  "font_normal": {
    "fontSize": 30
  },
  "theme_color": {
    "color": "#1296db"
  },
  "theme_bg": {
    "color": "#1296db"
  },
  "mask": {
    "backgroundColor": "#000000",
    "opacity": 0.6,
    "position": "absolute",
    "left": 0,
    "top": 0,
    "bottom": 0,
    "right": 0
  }
}

/***/ }),

/***/ 15:
/***/ (function(module, exports) {

module.exports = {
  "text": {
    "fontSize": "50",
    "textAlign": "center",
    "color": "#41B883"
  }
}

/***/ }),

/***/ 16:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//


exports.default = {
    props: {
        placeholder: {
            default: ''

        },
        placeholder_color: {
            default: '#ffffff'

        },
        color: {
            default: '#000000'

        },
        value: {
            default: ''
        },

        type: {
            type: String,
            default: 'text'
        },
        font_size: {
            default: 20
        },
        autofocus: {
            default: false
        },
        return_key_type: {
            default: 'defalut'
        }

    },
    data: function data() {
        return {

            pulldistance: 180,
            visiable: true

        };
    },

    methods: {
        onchange: function onchange(event) {
            this.visiable = !event.value == '';
            //                this.$emit('onchange',event.value);
            this.value = event.value;
            //                this.name="xxx"
        },
        onfocus: function onfocus() {
            this.$emit('focus');
        },
        focus: function focus() {
            this.$refs.input.focus();
        },
        blur: function blur() {
            this.$refs.input.blur();
            this.$emit('blur');
        },
        oninput: function oninput(e) {

            //                this.$emit('oninput');
            this.value = e.value;
            this.visiable = e.value != '';
            this.$emit('onchange', e.value);
        },
        onreturn: function onreturn(e) {
            this.$emit('return', e);
        },
        onclose: function onclose() {
            this.value = '';
            this.visiable = false;
            this.$emit('onchange', '');
        }
    },

    created: function created() {
        var globalEvent = weex.requireModule('globalEvent');
        globalEvent.addEventListener("onPageInit", function (e) {});

        this.visiable = !this.value == '';
    },
    ready: function ready() {}
};

/***/ }),

/***/ 17:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      flexDirection: "row",
      height: "100",
      alignItems: "center"
    }
  }, [_c('input', {
    ref: "input",
    staticStyle: {
      flex: "1",
      paddingLeft: "20",
      height: "100"
    },
    style: {
      'color': _vm.color,
      'placeholder-color': _vm.placeholder_color
    },
    attrs: {
      "returnKeyType": _vm.return_key_type,
      "autofocus": _vm.autofocus,
      "placeholder": _vm.placeholder,
      "type": _vm.type,
      "value": (_vm.value)
    },
    on: {
      "return": _vm.onreturn,
      "focus": _vm.onfocus,
      "change": _vm.onchange,
      "input": [function($event) {
        _vm.value = $event.target.attr.value
      }, _vm.oninput]
    }
  }), (_vm.visiable) ? _c('div', {
    staticStyle: {
      width: "50px",
      height: "100px",
      marginRight: "10",
      alignItems: "center",
      justifyContent: "center"
    },
    on: {
      "click": function($event) {
        _vm.onclose()
      }
    }
  }, [_c('image', {
    staticStyle: {
      width: "30px",
      height: "30px"
    },
    attrs: {
      "src": "root:img/delete.png"
    }
  })]) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 18:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(15)
)

/* script */
__vue_exports__ = __webpack_require__(16)

/* template */
var __vue_template__ = __webpack_require__(17)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/component/input.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-03e71907"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports = {
  "layout": {
    "backgroundColor": "#333333",
    "height": 128,
    "width": 750,
    "flexDirection": "row",
    "alignItems": "center",
    "justifyContent": "center"
  }
}

/***/ }),

/***/ 27:
/***/ (function(module, exports) {

module.exports = {
  "cancel": {
    "marginLeft": 20,
    "color": "#444444",
    "fontSize": 30,
    "fontWeight": "bold",
    "color:active": "#000000"
  }
}

/***/ }),

/***/ 28:
/***/ (function(module, exports) {

module.exports = {
  "bg": {
    "backgroundColor": "#f5f5f5"
  },
  "cell": {
    "height": 100,
    "backgroundColor": "#ffffff",
    "flexDirection": "row",
    "alignItems": "center",
    "borderRadius": 5
  },
  "arrow": {
    "width": 16,
    "height": 26
  },
  "font_normal": {
    "fontSize": 30
  },
  "theme_color": {
    "color": "#1296db"
  },
  "theme_bg": {
    "color": "#1296db"
  },
  "mask": {
    "backgroundColor": "#000000",
    "opacity": 0.6,
    "position": "absolute",
    "left": 0,
    "top": 0,
    "bottom": 0,
    "right": 0
  }
}

/***/ }),

/***/ 29:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var finput = __webpack_require__(18);
exports.default = {
    components: { finput: finput },
    props: {
        text: {
            type: String

        },
        radius: {
            default: 5
        },
        color: {
            type: String

        },
        value: {
            default: ''
        },
        placeholder: {
            default: ''

        },
        disabled: {

            default: false
        },
        autofocus: {
            default: true
        },
        needCancel: {
            default: true
        },
        type: {
            type: String,
            default: 'text'
        },
        font_size: {
            default: 20
        },
        placeholder_color: {
            default: '#8d909a'
        }

    },
    data: function data() {
        return {

            visiable: true

        };
    },

    methods: {
        cancel: function cancel() {
            this.$refs.input.blur();
            var nav = weex.requireModule('navigator');
            nav.backFull({}, false);
        },
        change: function change(res) {
            this.$emit('change', res);
        },
        onreturn: function onreturn(e) {
            this.$emit('return', e);
        },
        onfocus: function onfocus() {
            this.$emit('focus');
        },
        focus: function focus() {
            this.$refs.input.focus();
        },
        onblur: function onblur() {
            this.$emit('blur');
        },
        blur: function blur() {
            this.$refs.input.blur();
        },
        oninput: function oninput(e) {

            //                this.$emit('oninput');
            this.$emit('oninput', e);
            this.visiable = e.value != '';
        },
        onclick: function onclick() {
            if (!this.disabled) this.$emit('onclick');
        },
        panstart: function panstart() {
            if (!this.disabled) this.bgcolor = '#ff1b08';
        },
        panend: function panend() {
            if (!this.disabled) this.bgcolor = '#ff4800';
        },
        setenable: function setenable() {},
        onclose: function onclose() {
            this.value = '';
        }
    },

    created: function created() {

        this.visiable = !this.value == '';
    },
    ready: function ready() {}
    //        watch: {
    //
    //
    //            disabled:{
    //                immediate: true,
    //                handler (val) {
    //
    //                }
    //            }
    //        }
};

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


exports.default = {
    props: {
        title: {
            default: ''

        },
        back: {
            default: true
        },
        bgcolor: {
            default: '#222222'

        },
        isloading: {
            default: false
        },
        disabled: {

            default: false
        },

        type: {
            type: String,
            default: 'text'
        },
        font_size: {
            default: 20
        },
        height: {
            default: 128
        },
        top: {
            default: 40
        },
        titletop: {
            default: 10
        }

    },
    data: function data() {
        return {};
    },

    methods: {
        titleClick: function titleClick() {
            this.$emit('titleClick');
        },
        rightclick: function rightclick() {
            this.$emit('rightClick');
        },
        backTo: function backTo() {
            if (!this.back) {
                this.$emit('leftClick');
                return;
            }

            var nav = weex.requireModule("navigator");
            nav.back();
        },
        onclick: function onclick() {
            if (!this.disabled) this.$emit('onclick');
        },
        adjust: function adjust() {
            if (weex.config.env.platform == 'android') {
                //                    if(weex.config.env.osVersion=)
                var p = weex.config.env.osVersion;
                p = p.replace(/\./g, '');
                if (p.length < 3) p = p + "0";
                if (p <= '440') {
                    this.height = 108;
                    this.top = 16;
                    this.titletop = 4;
                }
            }
        }
    },

    created: function created() {

        this.adjust();
    },
    ready: function ready() {}
    //        watch: {
    //
    //
    //            disabled:{
    //                immediate: true,
    //                handler (val) {
    //
    //                }
    //            }
    //        }
};

/***/ }),

/***/ 30:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticStyle: {
      flexDirection: "row",
      alignItems: "center"
    }
  }, [_c('div', {
    staticStyle: {
      flex: "1",
      backgroundColor: "#5c616f",
      height: "65",
      borderRadius: "5",
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "row"
    },
    style: {
      'border-radius': _vm.radius
    }
  }, [_c('image', {
    staticStyle: {
      width: "40",
      height: "40",
      marginLeft: "20"
    },
    attrs: {
      "src": "root:img/search.png"
    }
  }), _c('finput', {
    ref: "input",
    staticStyle: {
      flex: "1",
      height: "65",
      paddingLeft: "1",
      marginTop: "5"
    },
    attrs: {
      "color": "#8d909a",
      "value": _vm.value,
      "return_key_type": "search",
      "placeholder_color": _vm.placeholder_color,
      "autofocus": _vm.autofocus,
      "placeholder": _vm.placeholder
    },
    on: {
      "return": _vm.onreturn,
      "onchange": _vm.change,
      "focus": _vm.onfocus,
      "blur": _vm.onblur
    }
  })], 1), (_vm.needCancel) ? _c('text', {
    staticClass: ["cancel"],
    on: {
      "click": _vm.cancel
    }
  }, [_vm._v("取消")]) : _vm._e()])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["layout"],
    style: {
      'background-color': _vm.bgcolor,
      'height': _vm.height
    }
  }, [_c('div', {
    staticStyle: {
      flexDirection: "row"
    },
    style: {
      'top': _vm.titletop
    }
  }, [(_vm.isloading) ? _c('div', {
    staticStyle: {
      height: "40",
      width: "40",
      marginRight: "10"
    }
  }) : _vm._e(), _c('text', {
    staticStyle: {
      flex: "1",
      color: "#ffffff",
      textAlign: "center",
      fontSize: "38"
    },
    on: {
      "click": _vm.titleClick
    }
  }, [_vm._v(_vm._s(_vm.title))]), (_vm.isloading) ? _c('floading', {
    staticStyle: {
      height: "40",
      width: "40",
      marginLeft: "10",
      marginTop: "5"
    },
    attrs: {
      "color": "#ffffff",
      "loadingStyle": "small"
    }
  }) : _vm._e()], 1), _c('div', {
    staticStyle: {
      width: "200",
      top: "40",
      position: "absolute",
      left: "0"
    },
    style: {
      'height': _vm.height,
      'top': _vm.top
    },
    on: {
      "click": _vm.backTo
    }
  }, [(_vm.back) ? _c('image', {
    staticStyle: {
      width: "80",
      height: "80"
    },
    attrs: {
      "src": "root:img/back.png"
    }
  }) : _vm._e(), _vm._t("left")], 2), _c('div', {
    staticStyle: {
      width: "200",
      position: "absolute",
      right: "0",
      top: "0",
      alignItems: "center",
      justifyContent: "center"
    },
    style: {
      'height': _vm.height
    },
    on: {
      "click": _vm.rightclick
    }
  }, [_vm._t("right")], 2), _c('div', {
    staticStyle: {
      height: "1",
      backgroundColor: "#111111",
      position: "absolute",
      bottom: "0",
      left: "0",
      right: "0"
    }
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 48:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(27)
)
__vue_styles__.push(__webpack_require__(28)
)

/* script */
__vue_exports__ = __webpack_require__(29)

/* template */
var __vue_template__ = __webpack_require__(30)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/component/searchbox.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-14136960"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(1)
)
__vue_styles__.push(__webpack_require__(2)
)

/* script */
__vue_exports__ = __webpack_require__(3)

/* template */
var __vue_template__ = __webpack_require__(4)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/component/header.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-c25810c0"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 95:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(96)
)
__vue_styles__.push(__webpack_require__(97)
)

/* script */
__vue_exports__ = __webpack_require__(98)

/* template */
var __vue_template__ = __webpack_require__(99)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/songjian/Code/weexplusproject/myfirst/src/native/busi/tab/mainpage.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-76a927c3"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 96:
/***/ (function(module, exports) {

module.exports = {
  "bg": {
    "backgroundColor": "#f5f5f5"
  },
  "cell": {
    "height": 100,
    "backgroundColor": "#ffffff",
    "flexDirection": "row",
    "alignItems": "center",
    "borderRadius": 5
  },
  "arrow": {
    "width": 16,
    "height": 26
  },
  "font_normal": {
    "fontSize": 30
  },
  "theme_color": {
    "color": "#1296db"
  },
  "theme_bg": {
    "color": "#1296db"
  },
  "mask": {
    "backgroundColor": "#000000",
    "opacity": 0.6,
    "position": "absolute",
    "left": 0,
    "top": 0,
    "bottom": 0,
    "right": 0
  }
}

/***/ }),

/***/ 97:
/***/ (function(module, exports) {

module.exports = {
  "indicator": {
    "width": "750",
    "height": "316",
    "position": "absolute",
    "itemColor": "#777777",
    "itemSelectedColor": "#ffffff",
    "itemSize": "20",
    "top": "120",
    "left": 1
  },
  "slider": {
    "width": "750",
    "height": "316"
  },
  "frame": {
    "width": "750",
    "height": "316",
    "position": "relative"
  },
  "image": {
    "width": "750",
    "height": "316"
  }
}

/***/ }),

/***/ 98:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var head = __webpack_require__(5);
var searchbox = __webpack_require__(48);
var net = __webpack_require__(0);
exports.default = {
    components: { head: head, searchbox: searchbox },
    data: function data() {
        return {

            serials: [],
            films: [],
            banners: [{ img: 'https://r1.ykimg.com/0510000059675180ADC0B0B35B056543' }, { img: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1503206028&di=36e9b5f41a2fe5cde117834c3166187c&imgtype=jpg&er=1&src=http%3A%2F%2Fwww.nhaowan.com%2Fuploadfile%2F2014%2F1201%2F20141201050553256.jpg' }, { img: 'https://r1.ykimg.com/0510000059671128AD9E07523604E53A' }, { img: 'https://r1.ykimg.com/05100000595E602EADC0B0ADA707B70A' }],
            searchtop: 20

        };
    },

    methods: {
        gotoserial: function gotoserial() {
            var notify = weex.requireModule("notify");
            notify.sendNative('tabselect', { name: '电视剧' });
        },
        gotomovie: function gotomovie() {
            var notify = weex.requireModule("notify");
            notify.sendNative('tabselect', { name: '电影' });
        },
        gotoBannerDetail: function gotoBannerDetail(item) {
            var n = weex.requireModule("navigator");
            var url = 'root:busi/detail.js';
            if (item.isSerial == '0') {
                url = 'root:busi/moviedetail.js';
            }
            n.pushParam(url, item);
        },
        gotoSerialDetail: function gotoSerialDetail(item) {
            var n = weex.requireModule("navigator");

            n.pushParam('root:busi/detail.js', item);
        },
        gotoFilmDetail: function gotoFilmDetail(item) {
            var n = weex.requireModule("navigator");
            n.pushParam('root:busi/moviedetail.js', item);
        },
        searchClick: function searchClick() {
            var n = weex.requireModule("navigator");
            n.pushParam('root:busi/searchList.js', { keyword: '梅' });
        },
        getleft: function getleft(index) {
            if (index % 3 == 0) {
                return 15;
            }
            return 7.5;
        },
        getright: function getright(index) {
            if (index % 3 == 2) {
                return 15;
            }
            return 7.5;
        },
        isandroid: function isandroid() {
            return weex.config.env.platform == 'android';
        },
        load: function load() {
            var _this = this;

            var p = {};
            var pref = weex.requireModule("pref");
            var r = pref.getObj('mainpagestr');
            if (r == undefined || r == '') {
                r = { "msg": "成功", "films": [{ "id": 32715, "name": "帅气的恶魔 Handsome Devil", "img": "http://img.hdwan.net/2017/08/p2455528649.jpg", "star": null, "type": 42, "total": null, "latestNo": null, "time": "2017-08-09 14:00:04", "isend": null, "sharp": 2, "country": " 爱尔兰", "typeName": "剧情", "sharpName": "HD" }, { "id": 32709, "name": "鸟鸣720P", "img": "http://img.hdwan.net/2014/06/e611_Birdsong.720P.jpg", "star": "", "type": 46, "total": 0, "latestNo": "0", "time": "2016-11-30 00:00:00", "isend": 0, "sharp": 2, "country": "", "typeName": "战争", "sharpName": "HD" }, { "id": 32708, "name": "反抗军", "img": "http://img.hdwan.net/2014/06/4dd5_Defiance.jpg", "star": "", "type": 46, "total": 0, "latestNo": "0", "time": "2016-11-30 00:00:00", "isend": 0, "sharp": 2, "country": "", "typeName": "战争", "sharpName": "HD" }, { "id": 32707, "name": "帝国陷落", "img": "http://img.hdwan.net/2014/06/eb69_The-Downfall.jpg", "star": "", "type": 46, "total": 0, "latestNo": "0", "time": "2016-11-30 00:00:00", "isend": 0, "sharp": 2, "country": "", "typeName": "战争", "sharpName": "HD" }, { "id": 32706, "name": "太阳帝国", "img": "http://img.hdwan.net/2014/06/64be_Empire-of-the-Sun.jpg", "star": "", "type": 46, "total": 0, "latestNo": "0", "time": "2016-11-30 00:00:00", "isend": 0, "sharp": 2, "country": "", "typeName": "战争", "sharpName": "HD" }, { "id": 32705, "name": "不列颠之战", "img": "http://img.hdwan.net/2014/06/03de_Battle-of-Britain.jpg", "star": "", "type": 46, "total": 0, "latestNo": "0", "time": "2016-11-30 00:00:00", "isend": 0, "sharp": 2, "country": "", "typeName": "战争", "sharpName": "HD" }, { "id": 32704, "name": "决战斯大林格勒", "img": "http://img.hdwan.net/2014/06/335f_Stalingrad.jpg", "star": "", "type": 46, "total": 0, "latestNo": "0", "time": "2016-11-30 00:00:00", "isend": 0, "sharp": 2, "country": "", "typeName": "战争", "sharpName": "HD" }, { "id": 32703, "name": "细细的红线", "img": "http://img.hdwan.net/2014/06/2271_The-Thin-Red-Line.jpg", "star": "", "type": 46, "total": 0, "latestNo": "0", "time": "2016-11-30 00:00:00", "isend": 0, "sharp": 2, "country": "", "typeName": "战争", "sharpName": "HD" }], "serials": [{ "id": 25832, "name": "逆转奇兵第四季", "img": "http://img.kukan5.com:808/pic/uploadimg/2017-6/p2457000333.jpg", "star": null, "type": 21, "total": 10, "latestNo": "9", "time": "2017-08-10 10:00:00", "isend": 0, "sharp": 2, "country": "美国", "typeName": "战争", "sharpName": "HD" }, { "id": 28215, "name": "摇滚学校第三季", "img": "http://img.kukan5.com:808/pic/uploadimg/2017-8/b_840f9c909d4f903f46f5e5bcc85edd1b.jpg", "star": null, "type": 18, "total": -1, "latestNo": "3", "time": "2017-08-10 09:00:02", "isend": 0, "sharp": 2, "country": "美国", "typeName": "歌舞", "sharpName": "HD" }, { "id": 28180, "name": "生死狙击第二季", "img": "http://img.kukan5.com:808/pic/uploadimg/2017-7/p2493379674.jpg", "star": null, "type": 35, "total": 10, "latestNo": "4", "time": "2017-08-10 09:00:02", "isend": 0, "sharp": 2, "country": "美国", "typeName": "动作", "sharpName": "HD" }, { "id": 28178, "name": "神秘礼物第一季", "img": "http://img.kukan5.com:808/pic/uploadimg/2017-7/b_8ac96d3ae3f7e0dbde4a562030d4a256.jpg", "star": null, "type": 13, "total": 10, "latestNo": "4", "time": "2017-08-10 09:00:02", "isend": 0, "sharp": 2, "country": "美国", "typeName": "罪案", "sharpName": "HD" }, { "id": 28159, "name": "青年莎士比亚第一季", "img": "http://img.kukan5.com:808/pic/uploadimg/2017-7/aefee074550cb9a4d754e70851c449a9.jpg", "star": null, "type": 36, "total": 10, "latestNo": "7", "time": "2017-08-10 09:00:02", "isend": 0, "sharp": 2, "country": "美国", "typeName": "剧情", "sharpName": "HD" }, { "id": 28179, "name": "如此一家人第五季", "img": "http://img.kukan5.com:808/pic/uploadimg/2017-7/b_6e71dd701afb7d620ec4574b9289602d.jpg", "star": null, "type": 6, "total": 20, "latestNo": "5", "time": "2017-08-10 09:00:01", "isend": 0, "sharp": 2, "country": "美国", "typeName": "青春", "sharpName": "HD" }, { "id": 28203, "name": "侦探双雄第一季", "img": "http://img.kukan5.com:808/pic/uploadimg/2017-8/p2494297355.jpg", "star": null, "type": 35, "total": 6, "latestNo": "5", "time": "2017-08-10 09:00:01", "isend": 1, "sharp": 2, "country": "美国", "typeName": "动作", "sharpName": "HD" }, { "id": 28211, "name": "哈啦夏令营第二季", "img": "http://img.kukan5.com:808/pic/uploadimg/2017-8/p2492742007.jpg", "star": null, "type": 27, "total": 8, "latestNo": "4", "time": "2017-08-10 09:00:00", "isend": 0, "sharp": 2, "country": "美国", "typeName": "喜剧", "sharpName": "HD" }], "err": 0, "banners": [{ "id": 28044, "name": "冰与火之歌第七季", "img": "http://img.kukan5.com:808/pic/uploadimg/2017-3/p2389329131.jpg", "star": null, "type": 5, "total": 7, "latestNo": "4", "time": "2017-08-07 18:00:00", "isend": 0, "sharp": 2, "isSerial": 1, "country": "美国", "banner_img": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1503206028&di=36e9b5f41a2fe5cde117834c3166187c&imgtype=jpg&er=1&src=http%3A%2F%2Fwww.nhaowan.com%2Fuploadfile%2F2014%2F1201%2F20141201050553256.jpg", "typeName": "魔幻", "sharpName": "HD" }, { "id": 26373, "name": "地球百子第四季", "img": "http://img.kukan5.com:808/pic/uploadimg/2017-2/001861c36e05205b461815337cd77d4f.jpg", "star": null, "type": 15, "total": 13, "latestNo": "13", "time": "2017-07-22 17:07:57", "isend": 1, "sharp": 2, "isSerial": 1, "country": "美国", "banner_img": "https://timgsa.baidu.com/timg?image&quality=100&size=b9999_10000&sec=1502611585798&di=6bc9e6062496e88106c5831f4715a8a8&imgtype=0&src=http%3A%2F%2Fcdn.iciba.com%2Fnews%2F2014%2F0821%2F20140821023015617.jpg", "typeName": "科幻", "sharpName": "HD" }, { "id": 26424, "name": "太空无垠第一季", "img": "http://img.kukan5.com:808/pic/uploadimg/2016-11/p2276966687.jpg", "star": null, "type": 15, "total": 10, "latestNo": "10", "time": "2017-07-22 17:07:59", "isend": 1, "sharp": 2, "isSerial": 1, "country": "美国", "banner_img": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1502611668507&di=15ae77107e21c9d5aae9939f124f17fb&imgtype=0&src=http%3A%2F%2Fimg0.178.com%2Fnewgame%2F201605%2F258328385035%2F258329155164.jpg", "typeName": "科幻", "sharpName": "HD" }] };
            }
            this.banners = r.banners;
            this.films = r.films;
            this.serials = r.serials;
            net.postSilent('recommand.do', p, function (res) {

                pref.setObj('mainpagestr', res);
                _this.banner = res.banners;
                _this.films = res.films;
                _this.serials = res.serials;
            });
        }
    },

    created: function created() {

        this.load();
        var globalEvent = weex.requireModule('globalEvent');
        globalEvent.addEventListener("onPageInit", function (e) {});
    },
    ready: function ready() {}
    //        watch: {
    //
    //
    //            disabled:{
    //                immediate: true,
    //                handler (val) {
    //
    //                }
    //            }
    //        }
};

/***/ }),

/***/ 99:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('head', {
    attrs: {
      "title": "推荐",
      "back": false
    },
    on: {
      "titleClick": _vm.gotoSearch
    }
  }), _c('scroller', {
    staticStyle: {
      backgroundColor: "#333333"
    }
  }, [_c('div', {
    staticStyle: {
      flexDirection: "row",
      height: "90",
      backgroundColor: "#444444"
    }
  }, [_c('div', {
    staticStyle: {
      width: "710",
      backgroundColor: "#5c616f",
      height: "55",
      marginTop: "20",
      marginLeft: "20",
      borderRadius: "30",
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "row"
    },
    on: {
      "click": _vm.searchClick
    }
  }, [_c('image', {
    staticStyle: {
      width: "40",
      height: "40",
      marginLeft: "20"
    },
    attrs: {
      "src": "root:img/search.png"
    }
  }), _c('div', {
    staticStyle: {
      height: "65",
      paddingLeft: "1",
      marginLeft: "10",
      justifyContent: "center",
      alignItems: "center"
    },
    style: {
      'margin-top': _vm.isandroid() ? 5 : 20
    }
  }, [_c('text', {
    staticStyle: {
      color: "#8d909a",
      fontSize: "30"
    }
  }, [_vm._v("冰与火")])])])]), _c('slider', {
    ref: "slider",
    staticClass: ["slider"],
    attrs: {
      "interval": "3000",
      "autoPlay": "true"
    }
  }, [_vm._l((_vm.banners), function(item) {
    return _c('div', {
      staticClass: ["frame"],
      on: {
        "click": function($event) {
          _vm.gotoBannerDetail(item)
        }
      }
    }, [_c('image', {
      staticClass: ["image"],
      attrs: {
        "resize": "cover",
        "placeholder": "root:img/default.png",
        "src": item.banner_img
      }
    })])
  }), _c('indicator', {
    staticClass: ["indicator"]
  })], 2), _c('div', [_c('div', {
    staticStyle: {
      flexDirection: "row"
    }
  }, [_c('text', {
    staticStyle: {
      fontSize: "33",
      fontWeight: "bold",
      color: "#ffffff",
      marginLeft: "20",
      marginTop: "20"
    }
  }, [_vm._v("电视剧")]), _c('div', {
    staticStyle: {
      flex: "1"
    }
  }), _c('text', {
    staticStyle: {
      fontSize: "30",
      fontWeight: "bold",
      color: "#808080",
      marginRight: "25",
      marginTop: "20"
    },
    on: {
      "click": function($event) {
        _vm.gotoserial()
      }
    }
  }, [_vm._v("更多")])]), _c('scroller', {
    staticStyle: {
      width: "750",
      flexDirection: "row",
      height: "380",
      marginTop: "10"
    },
    attrs: {
      "scrollDirection": "horizontal",
      "showScrollbar": "false",
      "bounce": "false"
    }
  }, _vm._l((_vm.serials), function(item) {
    return _c('div', {
      staticStyle: {
        padding: "0",
        marginLeft: "20"
      },
      on: {
        "click": function($event) {
          _vm.gotoSerialDetail(item)
        }
      }
    }, [_c('div', [_c('image', {
      staticStyle: {
        width: "220",
        height: "330"
      },
      attrs: {
        "resize": "cover",
        "placeholder": "root:img/default.png",
        "src": item.img
      }
    }), _c('div', {
      staticStyle: {
        alignItems: "center",
        justifyContent: "center",
        width: "100",
        height: "50",
        backgroundColor: "#000000",
        opacity: "0.7",
        position: "absolute",
        bottom: "0",
        right: "0",
        borderRadius: "0"
      }
    }), _c('div', {
      staticStyle: {
        alignItems: "center",
        justifyContent: "center",
        width: "100",
        height: "50",
        position: "absolute",
        bottom: "0",
        right: "0",
        borderRadius: "0"
      }
    }, [(item.isend == 0) ? _c('text', {
      staticStyle: {
        color: "#ffffff",
        fontSize: "28"
      }
    }, [_vm._v("至" + _vm._s(item.latestNo) + "集")]) : _vm._e(), (item.isend == 1) ? _c('text', {
      staticStyle: {
        color: "#ffffff",
        fontSize: "28"
      }
    }, [_vm._v("共" + _vm._s(item.total) + "集")]) : _vm._e()])]), _c('text', {
      staticStyle: {
        color: "#ffffff",
        fontSize: "25",
        marginTop: "10",
        textAlign: "center",
        maxWidth: "200"
      }
    }, [_vm._v(_vm._s(item.name))])])
  }))]), _c('div', [_c('div', {
    staticStyle: {
      flexDirection: "row"
    }
  }, [_c('text', {
    staticStyle: {
      fontSize: "33",
      fontWeight: "bold",
      color: "#ffffff",
      marginLeft: "20",
      marginTop: "20"
    }
  }, [_vm._v("电影")]), _c('div', {
    staticStyle: {
      flex: "1"
    }
  }), _c('text', {
    staticStyle: {
      fontSize: "30",
      fontWeight: "bold",
      color: "#808080",
      marginRight: "25",
      marginTop: "20"
    },
    on: {
      "click": function($event) {
        _vm.gotomovie()
      }
    }
  }, [_vm._v("更多")])]), _c('scroller', {
    staticStyle: {
      width: "750",
      flexDirection: "row",
      height: "380",
      marginTop: "10"
    },
    attrs: {
      "scrollDirection": "horizontal",
      "showScrollbar": "false",
      "bounce": "false"
    }
  }, _vm._l((_vm.films), function(item) {
    return _c('div', {
      staticStyle: {
        padding: "0",
        marginLeft: "20"
      },
      on: {
        "click": function($event) {
          _vm.gotoFilmDetail(item)
        }
      }
    }, [_c('div', [_c('image', {
      staticStyle: {
        width: "220",
        height: "330"
      },
      attrs: {
        "placeholder": "root:img/default.png",
        "src": item.img
      }
    }), _c('div', {
      staticStyle: {
        alignItems: "center",
        justifyContent: "center",
        width: "100",
        height: "50",
        backgroundColor: "#000000",
        opacity: "0.7",
        position: "absolute",
        bottom: "0",
        right: "0",
        borderRadius: "0"
      }
    }), _c('div', {
      staticStyle: {
        alignItems: "center",
        justifyContent: "center",
        width: "100",
        height: "50",
        position: "absolute",
        bottom: "0",
        right: "0",
        borderRadius: "0"
      }
    }, [_c('text', {
      staticStyle: {
        color: "#ffffff",
        fontSize: "28"
      }
    }, [_vm._v(_vm._s(item.sharpName))])])]), _c('text', {
      staticStyle: {
        color: "#ffffff",
        fontSize: "25",
        marginTop: "10",
        textAlign: "center",
        maxWidth: "200"
      }
    }, [_vm._v(_vm._s(item.name))])])
  }))])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ })

/******/ });